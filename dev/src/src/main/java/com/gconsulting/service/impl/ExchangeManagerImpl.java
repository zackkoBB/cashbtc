package com.gconsulting.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gconsulting.Constants;
import com.gconsulting.dao.ExchangeDao;
import com.gconsulting.model.API;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.HistoricalData;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiId;
import com.gconsulting.model.ids.FeeApiTypeId;
import com.gconsulting.service.ExchangeManager;

/**
 * Implementation of ExchangeManager interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Service("exchangeManager")
public class ExchangeManagerImpl implements ExchangeManager {

	private ExchangeDao exchangeDao;

	public ExchangeManagerImpl() {
	}

	public ExchangeManagerImpl(ExchangeDao exchangeDao) {
		this.exchangeDao = exchangeDao;
	}

	@Override
	@Autowired
	public void setExchangeDao(final ExchangeDao exchangeDao) {
		this.exchangeDao = exchangeDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Exchange getExchangeByCode(String code) {
		return exchangeDao.getExchangeByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Exchange> getAllExchange() {
		return exchangeDao.getAllExchange();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Exchange exchange) {
		exchangeDao.create(exchange);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Exchange exchange) {
		exchangeDao.update(exchange);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Exchange exchange) {
		exchangeDao.delete(exchange);
	}

	/**
	 * {@inheritDoc}
	 */
	public Market getMarketByCode(String code) {

		return exchangeDao.getMarketByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Market> getAllMarket() {
		return exchangeDao.getAllMarket();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Market market) {
		exchangeDao.create(market);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Market market) {
		exchangeDao.update(market);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Market market) {
		exchangeDao.delete(market);
	}

	/**
	 * {@inheritDoc}
	 */
	public API getApiById(FeeApiId id) {

		return exchangeDao.getApiById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<API> getApiByExchange(Exchange exchange) {
		return exchangeDao.getApiByExchange(exchange);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<API> getAllApi() {
		return exchangeDao.getAllApi();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(API api) {

		// Check if type exist
		// if (exchangeDao.getTypeById(new FeeApiTypeId(
		// api.getFeeType().getType(), api.getFeeType().getMarket())) == null) {
		// create(new FeeApiType(api.getFeeType().getType(), api.getFeeType()
		// .getMarket()));
		// }
		exchangeDao.create(api);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(API api) {
		exchangeDao.update(api);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(API api) {
		exchangeDao.delete(api);
	}

	/**
	 * {@inheritDoc}
	 */
	public Fee getFeeById(FeeApiId id) {

		return exchangeDao.getFeeById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Fee> getAllFee() {
		return exchangeDao.getAllFee();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Fee> getFeeByExchangeType(Exchange exchange, String type) {
		return exchangeDao.getFeeByExchangeType(exchange, type);
	}

	/**
	 * {@inheritDoc}
	 */
	public Map<String, Fee> getFIATTradingFee(List<Exchange> exchanges) {

		Map<String, Fee> result = new HashMap<String, Fee>();
		for (Exchange exchange : exchanges) {
			List<Fee> tradingFeesForExchange = exchangeDao
					.getFeeByExchangeType(exchange, Constants.FEE_TYPE_TRADE);
			Fee tradingFee = null;
			for (Fee tradingFeeForExchange : tradingFeesForExchange) {
				if (tradingFeeForExchange.getFeeType().getMarket().getCode()
						.equalsIgnoreCase(Constants.FEE_TRADE1)) {
					tradingFee = tradingFeeForExchange;
					break;
				}
			}
			result.put(exchange.getCode(), tradingFee);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public Map<String, Fee> getCRYPTOWithdrawFee(List<Exchange> exchanges) {

		Map<String, Fee> result = new HashMap<String, Fee>();
		for (Exchange exchange : exchanges) {
			List<Fee> transferFeesForExchange = exchangeDao
					.getFeeByExchangeType(exchange, Constants.FEE_TYPE_WITHDRAW);
			Fee cryptoWithdrawFee = null;
			for (Fee transferFeeForExchange : transferFeesForExchange) {
				if (transferFeeForExchange.getFeeType().getMarket().getCode()
						.equalsIgnoreCase(Constants.FEE_TRADE2)) {
					cryptoWithdrawFee = transferFeeForExchange;
					break;
				}
			}
			/*
			 * If no Fee is found put the default BTC withdraw fee (network fee)
			 */
			if (cryptoWithdrawFee == null) {
				cryptoWithdrawFee = new Fee(exchange, new FeeApiType(
						Constants.FEE_TYPE_WITHDRAW, new Market(
								Constants.FEE_TRADE2, "")),
						Constants.FEE_UNIT_BTC,
						Constants.FEE_BTC_NETWORK_FEE.toString(),
						Constants.FEE_BTC_NETWORK_FEE_NOTE);
			}
			result.put(exchange.getCode(), cryptoWithdrawFee);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Fee fee) {
		exchangeDao.create(fee);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Fee fee) {
		exchangeDao.update(fee);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Fee fee) {
		exchangeDao.delete(fee);
	}

	/**
	 * {@inheritDoc}
	 */
	public FeeApiType getTypeById(FeeApiTypeId id) {
		return exchangeDao.getTypeById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<FeeApiType> getTypeByType(String type) {
		return exchangeDao.getTypeByType(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<FeeApiType> getAllType() {
		return exchangeDao.getAllType();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(FeeApiType type) {

		// Check if Market exist
		// if (exchangeDao.getMarketByCode(type.getMarket().getCode()) == null)
		// {
		// create(new Market(type.getMarket().getCode(), type.getMarket()
		// .getCode() + " market"));
		// }
		exchangeDao.create(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(FeeApiType type) {
		exchangeDao.update(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(FeeApiType type) {
		exchangeDao.delete(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public HistoricalData getHistoricalDataById(Long id) {
		return exchangeDao.getHistoricalDataById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<HistoricalData> getAllHistoricalData() {
		return exchangeDao.getAllHistoricalData();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<HistoricalData> getAllHistoricalDataByExchange(Exchange exchange) {
		return exchangeDao.getAllHistoricalDataByExchange(exchange);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<HistoricalData> getAllHistoricalDataByExchangeAndType(
			Exchange exchange, FeeApiType type) {
		return exchangeDao
				.getAllHistoricalDataByExchangeAndType(exchange, type);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Long> getHistoricalDataDataByExchangeAndType(Exchange exchange,
			FeeApiType type) {

		Object[] queryResult = exchangeDao
				.getHistoricalDataDataByExchangeAndType(exchange, type);
		if (queryResult != null) {
			List<Long> result = new ArrayList<>();
			result.add((Long) queryResult[0]);
			result.add((Long) queryResult[1]);
			result.add((Long) queryResult[2]);
			return result;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Long getHistoricalDataMaxTimeByExchangeAndType(Exchange exchange,
			FeeApiType type) {
		return exchangeDao.getHistoricalDataMaxTimeByExchangeAndType(exchange,
				type);
	}

	/**
	 * {@inheritDoc}
	 */
	public HistoricalData getHistoricalDataByExchangeTypeAndTimestamp(
			Exchange exchange, FeeApiType type, Long timestamp) {
		return exchangeDao.getHistoricalDataByExchangeTypeAndTimestamp(
				exchange, type, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(HistoricalData historicalData) {
		exchangeDao.create(historicalData);
		;
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(HistoricalData historicalData) {
		exchangeDao.update(historicalData);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(HistoricalData historicalData) {
		exchangeDao.delete(historicalData);
	}
}
