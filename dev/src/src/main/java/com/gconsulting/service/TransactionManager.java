package com.gconsulting.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.dao.TransactionDao;
import com.gconsulting.model.Transaction;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface TransactionManager extends
		GenericManager<Transaction, String>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	void setTransactionDao(TransactionDao transactionDao);

	/**
	 * Gets Transaction information based on code.
	 * 
	 * @param Code
	 *            code of the Transaction to be retrieved
	 * @return Transaction retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Transaction getTransactionByCode(String code);

	/**
	 * Gets Transaction information based on address.
	 * 
	 * @param address
	 *            address of the Transaction to be retrieved
	 * @return List<Transaction> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionByAddress(String address);

	/**
	 * Gets all Transaction entities in the db
	 * 
	 * @return List<Transaction> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getAll();

	/**
	 * Create a new Transaction
	 * 
	 * @throws
	 */
	@Transactional
	void create(Transaction transaction);

	/**
	 * Update an existing Transaction instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Transaction transaction);

	/**
	 * Delete an existing Transaction instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Transaction transaction);
}
