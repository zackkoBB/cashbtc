package com.gconsulting.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gconsulting.dao.ConfigurationDao;
import com.gconsulting.model.Configuration;
import com.gconsulting.service.ConfigurationManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("configurationManager")
public class ConfigurationManagerImpl extends
		GenericManagerImpl<Configuration, String> implements
		ConfigurationManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 836547800984512216L;
	private ConfigurationDao configurationDao;

	@Override
	@Autowired
	public void setConfigurationDao(final ConfigurationDao configurationDao) {
		this.dao = configurationDao;
		this.configurationDao = configurationDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Configuration getConfigurationByKey(String key) {
		return this.configurationDao.getConfigurationByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Configuration configuration) {
		this.configurationDao.create(configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Configuration configuration) {
		this.configurationDao.update(configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Configuration configuration) {
		this.configurationDao.delete(configuration);
	}
}
