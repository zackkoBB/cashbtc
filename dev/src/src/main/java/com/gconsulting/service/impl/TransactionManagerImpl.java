package com.gconsulting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gconsulting.dao.TransactionDao;
import com.gconsulting.model.Transaction;
import com.gconsulting.service.TransactionManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("orderTransactionManager")
public class TransactionManagerImpl extends
		GenericManagerImpl<Transaction, String> implements TransactionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8253446758121224955L;
	private TransactionDao transactionDao;

	@Override
	@Autowired
	public void setTransactionDao(final TransactionDao transactionDao) {
		this.dao = transactionDao;
		this.transactionDao = transactionDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByCode(String code) {

		// Query qry = getSession().createQuery(
		// "from Exchange e where e.code='" + code + "'");
		// if (qry.list() != null) {
		// if (qry.list().size() > 0) {
		// return (Exchange) qry.list().get(0);
		// }
		// }
		// return null;
		return this.transactionDao.getTransactionByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAddress(String address) {
		return transactionDao.getTransactionByAddress(address);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Transaction transaction) {
		this.transactionDao.create(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Transaction transaction) {
		this.transactionDao.update(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Transaction transaction) {
		this.transactionDao.delete(transaction);
	}
}
