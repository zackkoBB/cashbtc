package com.gconsulting.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.dao.ConfigurationDao;
import com.gconsulting.model.Configuration;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface ConfigurationManager extends
		GenericManager<Configuration, String>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	void setConfigurationDao(ConfigurationDao configurationDao);

	/**
	 * Gets Configuration information based on code.
	 * 
	 * @param key
	 *            key of the Configuration to be retrieved
	 * @return Configuration retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Configuration getConfigurationByKey(String key);

	/**
	 * Gets all Configuration entities in the db
	 * 
	 * @return List<Configuration> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Configuration> getAll();

	/**
	 * Create a new Configuration
	 * 
	 * @throws
	 */
	@Transactional
	void create(Configuration configuration);

	/**
	 * Update an existing Configuration instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Configuration configuration);

	/**
	 * Delete an existing Configuration instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Configuration configuration);
}
