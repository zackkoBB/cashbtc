package com.gconsulting.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.dao.ExchangeDao;
import com.gconsulting.model.API;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.HistoricalData;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiId;
import com.gconsulting.model.ids.FeeApiTypeId;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:gsit80@gmail.com"></a>
 */
public interface ExchangeManager {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param exchangeDao
	 *            the ExchangeDao implementation to use
	 */
	public void setExchangeDao(ExchangeDao exchangeDao);

	/**
	 * Retrieves an Exchange by code. An exception is thrown if exchange not
	 * found
	 *
	 * @param code
	 *            the identifier for the Echange
	 * @return Echange
	 */
	public Exchange getExchangeByCode(String code);

	/**
	 * Gets all Exchange entities in the db
	 * 
	 * @return List<Exchange> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Exchange> getAllExchange();

	/**
	 * Create a new Exchange
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Exchange exchange);

	/**
	 * Update an existing Exchange instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Exchange exchange);

	/**
	 * Delete an existing Exchange instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Exchange exchange);

	/**
	 * Gets Market information based on code.
	 * 
	 * @param Code
	 *            code of the market to be retrieved
	 * @return Market retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Market getMarketByCode(String code);

	/**
	 * Gets all Market entities in the db
	 * 
	 * @return List<Market> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Market> getAllMarket();

	/**
	 * Create a new Market
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Market market);

	/**
	 * Update an existing Market instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Market market);

	/**
	 * Delete an existing Market instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Market market);

	/**
	 * Gets Api information based on code.
	 * 
	 * @param FeeApiId
	 *            id of the api to be retrieved
	 * @return API retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public API getApiById(FeeApiId id);

	/**
	 * Gets Api information based on the Exchange.
	 * 
	 * @param Exchange
	 *            exchange of the api to be retrieved
	 * @return List<Api> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<API> getApiByExchange(Exchange exchange);

	/**
	 * Gets all Api entities in the db
	 * 
	 * @return List<Api> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<API> getAllApi();

	/**
	 * Create a new Api
	 * 
	 * @throws
	 */
	@Transactional
	public void create(API api);

	/**
	 * Update an existing Api instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(API api);

	/**
	 * Delete an existing Api instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(API api);

	/**
	 * Gets Fee information based on code.
	 * 
	 * @param FeeApiId
	 *            id of the fee to be retrieved
	 * @return Fee retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Fee getFeeById(FeeApiId id);

	/**
	 * Gets all Fee entities in the db
	 * 
	 * @return List<Fee> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Fee> getAllFee();

	/**
	 * Gets all Fee entities in the db for a given Exchange/Type in input
	 * 
	 * @return List<Fee> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Fee> getFeeByExchangeType(Exchange exchange, String type);

	/**
	 * Gets FIAT trading Fee for a given list of Exchanges
	 * 
	 * @param List
	 *            <Exchange> exchanges for which the Fee need to be retrieved
	 * @return Map<String, Fee> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Map<String, Fee> getFIATTradingFee(List<Exchange> exchanges);

	/**
	 * Gets CRYPTO withdraw Fee for a given list of Exchanges
	 * 
	 * @param List
	 *            <Exchange> exchanges for which the Fee need to be retrieved
	 * @return Map<String, Fee> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Map<String, Fee> getCRYPTOWithdrawFee(List<Exchange> exchanges);

	/**
	 * Create a new Fee
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Fee fee);

	/**
	 * Update an existing Fee instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Fee fee);

	/**
	 * Delete an existing Fee instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Fee fee);

	/**
	 * Gets Type information based on ids.
	 * 
	 * @param FeeApiTypeId
	 *            id of the type to be retrieved
	 * @return FeeApiType retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public FeeApiType getTypeById(FeeApiTypeId id);

	/**
	 * Gets Type information based on type.
	 * 
	 * @param Type
	 *            of the type to be retrieved
	 * @return List<FeeApiType> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<FeeApiType> getTypeByType(String type);

	/**
	 * Gets all FeeApiType entities in the db
	 * 
	 * @return List<FeeApiType> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<FeeApiType> getAllType();

	/**
	 * Create a new FeeApiType
	 * 
	 * @throws
	 */
	@Transactional
	public void create(FeeApiType type);

	/**
	 * Update an existing FeeApiType instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(FeeApiType type);

	/**
	 * Delete an existing FeeApiType instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(FeeApiType type);

	/**
	 * Gets HistoricalData information based on ids.
	 * 
	 * @param Long
	 *            id of the type to be retrieved
	 * @return HistoricalData retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public HistoricalData getHistoricalDataById(Long id);

	/**
	 * Gets all HistoricalData entities in the db
	 * 
	 * @return List<HistoricalData> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<HistoricalData> getAllHistoricalData();

	/**
	 * Gets all HistoricalData entities in the db by Exchange
	 * 
	 * @param Exchange
	 *            Exchange from which HistoricaData needs to be retrieved
	 * @return List<HistoricalData> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<HistoricalData> getAllHistoricalDataByExchange(Exchange exchange);

	/**
	 * Gets all HistoricalData entities in the db by Exchange and Market
	 * 
	 * @param Exchange
	 *            Exchange from which HistoricaData needs to be retrieved
	 * @param FeeApiType
	 *            type from which HistoricaData needs to be retrieved
	 * @return List<HistoricalData> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<HistoricalData> getAllHistoricalDataByExchangeAndType(
			Exchange exchange, FeeApiType type);

	/**
	 * Gets HistoricalData data by Exchange and Market
	 * 
	 * @param Exchange
	 *            Exchange from which HistoricaData needs to be retrieved
	 * @param FeeApiType
	 *            type from which HistoricaData needs to be retrieved
	 * @return List<Long> rows count, unixtime startTime, unixtime endTime
	 * 
	 * @throws
	 */
	@Transactional
	public List<Long> getHistoricalDataDataByExchangeAndType(Exchange exchange,
			FeeApiType type);

	/**
	 * Get HistoricalData max time by Exchange and Market
	 * 
	 * @param Exchange
	 *            Exchange from which HistoricaData needs to be retrieved
	 * @param FeeApiType
	 *            type from which HistoricaData needs to be retrieved
	 * @return Long unixtime endTime
	 * 
	 * @throws
	 */
	@Transactional
	public Long getHistoricalDataMaxTimeByExchangeAndType(Exchange exchange,
			FeeApiType type);

	/**
	 * Gets HistoricalData by Exchange, type and timestamp
	 * 
	 * @param Exchange
	 *            Exchange from which HistoricaData needs to be retrieved
	 * @param FeeApiType
	 *            type from which HistoricaData needs to be retrieved
	 * @param Long
	 *            timestamp from which HistoricaData needs to be retrieved
	 * @return HistoricalDat> retrieved
	 * 
	 * @throws
	 */
	public HistoricalData getHistoricalDataByExchangeTypeAndTimestamp(
			Exchange exchange, FeeApiType type, Long timestamp);

	/**
	 * Create a new HistoricalData
	 * 
	 * @throws
	 */
	@Transactional
	public void create(HistoricalData historicalData);

	/**
	 * Update an existing HistoricalData instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(HistoricalData historicalData);

	/**
	 * Delete an existing HistoricalData instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(HistoricalData historicalData);
}
