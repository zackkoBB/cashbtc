package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*
* @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
*/
@Entity
@Table(name = "transaction")
public class Transaction extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1423187821848841631L;
	private String code;
	private String address;
	private Double amount;
	private Long timestamp;
	private String note;
	private Boolean fulfilled;
	
	
	public Transaction() {
		super();
	}

	public Transaction(String code, String address, Double amount, 
			Long timestamp, String note, Boolean fulfilled) {
		super();
		this.code = code;
		this.address = address;
		this.amount = amount;
		this.timestamp = timestamp;
		this.note = note;
		this.fulfilled = fulfilled;
	}

	@Id
	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "timestamp")
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	@Column(name = "note")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "fulfilled")
	public Boolean getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(Boolean fulfilled) {
		this.fulfilled = fulfilled;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
