package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "configuration")
public class Configuration extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8817048806145744044L;
	private String key;
	private String value;

	public Configuration() {
	}

	public Configuration(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	@Id
	@Column(name = "key", nullable = false, length = 20)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {

		ToStringBuilder sb = new ToStringBuilder(this,
				ToStringStyle.DEFAULT_STYLE).append("key", this.key).append(
				"value", this.value);
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}
		if (!(o instanceof Configuration)) {
			return false;
		}
		final Configuration market = (Configuration) o;
		return !(key != null ? !key.equals(market.getKey())
				: market.getValue() != null);
	}

	@Override
	public int hashCode() {
		return (key != null ? key.hashCode() : 0);
	}
}
