package com.gconsulting.webapp.model;

public class Order {

	private Double price;
	private Double quantity;
	private Double cumValue;

	public Order() {
		super();
	}

	public Order(Double price, Double quantity, Double cumValue) {
		super();
		this.price = price;
		this.quantity = quantity;
		this.cumValue = cumValue;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getCumValue() {
		return cumValue;
	}

	public void setCumValue(Double cumValue) {
		this.cumValue = cumValue;
	}
}
