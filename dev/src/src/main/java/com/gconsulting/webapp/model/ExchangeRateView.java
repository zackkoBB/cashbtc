package com.gconsulting.webapp.model;

public class ExchangeRateView {

	private String label;
	private Double value;
	private String colour;

	public ExchangeRateView() {
		super();
	}

	public ExchangeRateView(String label, Double value, String colour) {
		super();
		this.label = label;
		this.value = value;
		this.colour = colour;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

}
