package com.gconsulting.webapp.model;

import com.gconsulting.model.Exchange;
import com.gconsulting.model.FeeApiType;

public class HistoricalDataList {

	private Exchange exchange;
	private FeeApiType type;
	private Long records;
	private Long startTime;
	private Long endTime;

	public HistoricalDataList() {
		super();
	}

	public HistoricalDataList(Exchange exchange, FeeApiType type, Long records,
			Long startTime, Long endTime) {
		super();
		this.exchange = exchange;
		this.type = type;
		this.records = records;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public FeeApiType getType() {
		return type;
	}

	public void setType(FeeApiType type) {
		this.type = type;
	}

	public Long getRecords() {
		return records;
	}

	public void setRecords(Long records) {
		this.records = records;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

}
