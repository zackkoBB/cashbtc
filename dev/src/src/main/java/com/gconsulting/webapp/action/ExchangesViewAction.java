package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.Exchange;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.model.ExchangeView;

@SuppressWarnings("unchecked")
@Scope("request")
@Component("exchangesViewAction")
public class ExchangesViewAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9160499813064491537L;
	private ExchangeManager exchangeManager;
	private List<ExchangeView> exchanges1;
	private List<ExchangeView> exchanges2;

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public ExchangesViewAction() {
		setSortColumn("exchange.code"); // sets the default sort column
	}

	@PostConstruct
	public void init() {

		List<Exchange> exchanges = exchangeManager.getAllExchange();
		Exchange exchange;
		ExchangeView exchangeView;
		exchanges1 = new ArrayList<>();
		exchanges2 = new ArrayList<>();
		for (int i = 0; i < (exchanges.size() / 2); i++) {
			exchange = exchanges.get(i);
			exchangeView = new ExchangeView();
			exchangeView.setExchange(exchange);
			exchangeView.setTradingFees(exchangeManager.getFeeByExchangeType(
					exchange, Constants.FEE_TYPE_TRADE));
			exchangeView
					.setDepositMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_DEPOSIT));
			exchangeView
					.setWithdrawMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_WITHDRAW));
			exchanges1.add(exchangeView);

		}
		for (int i = (exchanges.size() / 2); i < exchanges.size(); i++) {
			exchange = exchanges.get(i);
			exchangeView = new ExchangeView();
			exchangeView.setExchange(exchange);
			exchangeView.setTradingFees(exchangeManager.getFeeByExchangeType(
					exchange, Constants.FEE_TYPE_TRADE));
			exchangeView
					.setDepositMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_DEPOSIT));
			exchangeView
					.setWithdrawMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_WITHDRAW));
			exchanges2.add(exchangeView);

		}
	}

	public List<ExchangeView> getExchanges1() {
		return exchanges1;
	}

	public void setExchanges1(List<ExchangeView> exchanges1) {
		this.exchanges1 = exchanges1;
	}

	public List<ExchangeView> getExchanges2() {
		return exchanges2;
	}

	public void setExchanges2(List<ExchangeView> exchanges2) {
		this.exchanges2 = exchanges2;
	}

	public List<ExchangeView> getExchanges() {

		List<ExchangeView> result = new ArrayList<>();
		List<Exchange> exchanges = exchangeManager.getAllExchange();
		for (Exchange exchange : exchanges) {
			ExchangeView exchangeView = new ExchangeView();
			exchangeView.setExchange(exchange);
			exchangeView.setTradingFees(exchangeManager.getFeeByExchangeType(
					exchange, Constants.FEE_TYPE_TRADE));
			exchangeView
					.setDepositMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_DEPOSIT));
			exchangeView
					.setWithdrawMethods(exchangeManager.getFeeByExchangeType(
							exchange, Constants.FEE_TYPE_WITHDRAW));
			result.add(exchangeView);
		}
		return sort(result);
	}
}
