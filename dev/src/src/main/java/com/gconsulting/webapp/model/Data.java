package com.gconsulting.webapp.model;

public class Data {

	private long time;
	private double price;
	private double quantity;

	public Data() {
		super();
	}

	public Data(long time, double price, double quantity) {
		super();
		this.time = time;
		this.price = price;
		this.quantity = quantity;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

}
