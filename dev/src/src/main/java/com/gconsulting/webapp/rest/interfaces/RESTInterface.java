package com.gconsulting.webapp.rest.interfaces;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.Orderbook;

public interface RESTInterface {

	public List<Market> getPairs(JSONObject source);
	public Orderbook getOrderbook(JSONObject source, String market) throws JSONException;
	public ExchangeRate getTicker(JSONObject source) throws JSONException;
}
