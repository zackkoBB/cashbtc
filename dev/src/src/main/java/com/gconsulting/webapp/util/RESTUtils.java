package com.gconsulting.webapp.util;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import com.gconsulting.Constants;

public class RESTUtils {

	private static Client client;

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final RESTUtils INSTANCE = new RESTUtils();

	private RESTUtils() {

		ClientConfig config = new ClientConfig();
		config = config.property(ClientProperties.CONNECT_TIMEOUT,
				Constants.REST_CONNECTION_TIMEOUT);
		config = config.property(ClientProperties.READ_TIMEOUT,
				Constants.REST_READ_TIMEOUT);
		client = ClientBuilder.newClient(config);
	}

	public static RESTUtils getInstance() {
		return INSTANCE;
	}

	/**
	 * Read a GET API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return String result
	 */
	public synchronized Response getAPI(String URI) {

		WebTarget target = client.target(getBaseURI(URI));
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		return builder.get();
	}

	private URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

}
