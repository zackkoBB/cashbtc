package com.gconsulting.webapp.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.gconsulting.model.Newsletter;
import com.gconsulting.service.NewsletterManager;
import com.gconsulting.service.impl.NewsletterManagerImpl;

@Scope("session")
@Controller("newsletterController")
public class NewsletterController extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1386508017320262457L;
	// private static final EMAIL_VALIDATOR =
	// "^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$";
	private NewsletterManager newsletterManager;

	public NewsletterController() {

		super();
		newsletterManager = new NewsletterManagerImpl();
	}

	@PostConstruct
	public void init() {
	}

	@Autowired
	public void setNewsletterManager(
			@Qualifier("newsletterManager") NewsletterManager newsletterManager) {
		this.newsletterManager = newsletterManager;
	}

	private void registerUserNewsletter(String email, String messagesComponentId) {

		/*
		 * TODO: (Optional): send confirmation email.
		 */
		try {
			if (email != null) {
				if (email.length() > 0) {
					if (EmailValidator.getInstance().isValid(email)) {
						if (newsletterManager.getNewsletter(email) != null) {
							addFacesError("website.home.newsletterExisting",
									null, messagesComponentId);
						} else {
							newsletterManager.saveNewsletter(new Newsletter(
									email));
							addFacesMessage("website.home.newsletterOK", null,
									messagesComponentId);
						}
					} else {
						addFacesError("website.home.newsletterNotValid",
								null, messagesComponentId);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception: " + e.getMessage());
			addFacesError("website.home.newsletterError", null,
					messagesComponentId);
		}
	}

	public void registerUserNewsletter() {
		registerUserNewsletter(new String(getParameter("subscribeEmail")),
				new String("confrimNewsletterSubscription"));
	}

	public void registerUserNewsletterSecond() {
		registerUserNewsletter(
				new String(getParameter("subscribeEmailSecond")), new String(
						"confrimNewsletterSubscriptionSecond"));
	}
}
