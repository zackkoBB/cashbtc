package com.gconsulting.webapp.model;

import java.util.List;

public class Orderbook {

	private List<Order> sellOrders;
	private List<Order> buyOrders;
	
	public Orderbook() {
		super();
	}

	public Orderbook(List<Order> sellOrders, List<Order> buyOrders) {
		super();
		this.sellOrders = sellOrders;
		this.buyOrders = buyOrders;
	}

	public List<Order> getSellOrders() {
		return sellOrders;
	}

	public void setSellOrders(List<Order> sellOrders) {
		this.sellOrders = sellOrders;
	}

	public List<Order> getBuyOrders() {
		return buyOrders;
	}

	public void setBuyOrders(List<Order> buyOrders) {
		this.buyOrders = buyOrders;
	}

}
