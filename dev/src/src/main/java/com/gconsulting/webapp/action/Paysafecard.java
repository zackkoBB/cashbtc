package com.gconsulting.webapp.action;

import java.io.Serializable;

public class Paysafecard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4133822403365589175L;
	private String pin1;
	private String pin2;
	private String pin3;
	private String pin4;
	private Double size;

	public Paysafecard() {
		super();
	}

	public Paysafecard(String pin1, String pin2, String pin3, String pin4,
			Double size) {
		super();
		this.pin1 = pin1;
		this.pin2 = pin2;
		this.pin3 = pin3;
		this.pin4 = pin4;
		this.size = size;
	}

	public String getPin1() {
		return pin1;
	}

	public void setPin1(String pin1) {
		this.pin1 = pin1;
	}

	public String getPin2() {
		return pin2;
	}

	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}

	public String getPin3() {
		return pin3;
	}

	public void setPin3(String pin3) {
		this.pin3 = pin3;
	}

	public String getPin4() {
		return pin4;
	}

	public void setPin4(String pin4) {
		this.pin4 = pin4;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public String getPIN() {
		return pin1 + pin2 + pin3 + pin4;
	}

	public String getPINSeparated() {
		return pin1 + "-" + pin2 + "-" + pin3 + "-" + pin4;
	}
}
