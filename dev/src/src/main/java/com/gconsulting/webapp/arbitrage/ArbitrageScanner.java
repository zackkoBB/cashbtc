package com.gconsulting.webapp.arbitrage;

import java.lang.Thread.State;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gconsulting.Constants;
import com.gconsulting.model.Configuration;
import com.gconsulting.service.ConfigurationManager;
import com.gconsulting.webapp.exception.ApplicationException;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.rest.interfaces.RESTInterface;

public class ArbitrageScanner {

	protected final Log log = LogFactory.getLog(getClass());

	private ExchangeRate exchangeRate;
	private ExchangeRate walletBalance;
	private Thread arbitrageThread;
	private Client client;
	private ConfigurationManager configurationManager;

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final ArbitrageScanner INSTANCE = new ArbitrageScanner();

	private ArbitrageScanner() {

		ClientConfig config = new ClientConfig();
		config = config.property(ClientProperties.CONNECT_TIMEOUT,
				Constants.REST_CONNECTION_TIMEOUT);
		config = config.property(ClientProperties.READ_TIMEOUT,
				Constants.REST_READ_TIMEOUT);
		client = ClientBuilder.newClient(config);
		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						// e.printStackTrace();
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	public static ArbitrageScanner getInstance() {
		return INSTANCE;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	/**
	 * Read a GET API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return String result
	 */
	private String getAPI(String URI) {

		WebTarget target = client.target(getBaseURI(URI));
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	public boolean arbitrageThreadIsAlive() {
		return arbitrageThread.isAlive();
	}

	public boolean arbitrageThreadIsInterrupted() {
		return arbitrageThread.isInterrupted();
	}

	public void arbitrageThreadCheckAccess() {
		arbitrageThread.checkAccess();
	}

	public State arbitrageThreadState() {
		return arbitrageThread.getState();
	}

	public void arbitrageThreadRestart() {

		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	public ExchangeRate getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(ExchangeRate exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public ExchangeRate getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(ExchangeRate walletBalance) {
		this.walletBalance = walletBalance;
	}

	/**
	 * ArbitrageScannerThread for retrieving arbs opportunities across different
	 * markets
	 * 
	 * @author zackko
	 *
	 */
	private class ArbitrageScannerThread implements Runnable {

		private boolean stop = false;
		private List<String> walletAddresses;
		private String exchange;
		private String exchangeRateAPI;
		private String walletBalanceExchange;
		private Integer restArbitragePollingInterval;

		/**
		 * Init usdMarket and sources for retrieving arbs
		 * 
		 */
		private void init() throws ApplicationException {

			walletAddresses = new ArrayList<>();
			if (configurationManager != null) {
				int i = 0;
				Configuration walletAddress = configurationManager
						.getConfigurationByKey(Constants.WALLET_BALANCE_ADDRESS_PREFIX
								+ (new Integer(i)).toString());
				String walletBalanceAPI = configurationManager
						.getConfigurationByKey(Constants.WALLET_BALANCE_API)
						.getValue();
				while (walletAddress != null) {
					walletAddresses.add(walletBalanceAPI
							+ walletAddress.getValue());
					i++;
					walletAddress = configurationManager
							.getConfigurationByKey(Constants.WALLET_BALANCE_ADDRESS_PREFIX
									+ (new Integer(i)).toString());
				}
				exchange = configurationManager.getConfigurationByKey(
						Constants.EXCHANGE_RATE_EXCHANGE).getValue();
				exchangeRateAPI = configurationManager.getConfigurationByKey(
						Constants.EXCHANGE_RATE_API).getValue();
				walletBalanceExchange = configurationManager
						.getConfigurationByKey(
								Constants.WALLET_BALANCE_EXCHANGE).getValue();
				restArbitragePollingInterval = new Integer(configurationManager
						.getConfigurationByKey(
								Constants.REST_ARBITRAGE_POLLING_INTERVAL)
						.getValue());
			} else {
				throw new ApplicationException("configurationManager is Null");
			}
		}

		/**
		 * Get exchange rate marketPrefix from web
		 * 
		 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
		 *         retrieved
		 */
		private ExchangeRate getExchangeRate(String exchange, String api) {

			try {
				// log.info(") " + api);
				JSONObject response = new JSONObject(getAPI(api));
				Class<?> restInterfaceClass = Class
						.forName(Constants.REST_BASE_PACKAGE + "." + exchange
								+ "Interface");
				RESTInterface restInterface = (RESTInterface) restInterfaceClass
						.newInstance();
				return restInterface.getTicker(response);
			} catch (Exception e) {
				log.error("Cannot get the ExchangeRate: " + api + " "
						+ e.getMessage());
				// e.printStackTrace();
				return null;
			}
		}

		/**
		 * Get exchange rate marketPrefix from web
		 * 
		 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
		 *         retrieved
		 */
		private ExchangeRate getWalletBalance(String exchange, List<String> apis) {

			double result = 0.0;
			for (String api : apis) {
				try {
					// log.info(") " + api);
					JSONObject response = new JSONObject(getAPI(api));
					Class<?> restInterfaceClass = Class
							.forName(Constants.REST_BASE_PACKAGE + "."
									+ exchange + "Interface");
					RESTInterface restInterface = (RESTInterface) restInterfaceClass
							.newInstance();
					result += restInterface.getTicker(response).getRate();
				} catch (Exception e) {
					log.error("Cannot get the ExchangeRate: " + api + " "
							+ e.getMessage());
					// e.printStackTrace();
					return null;
				}
			}
			return new ExchangeRate(0L, result);
		}

		/**
		 * Run method of this thread
		 */
		public void run() {

			try {
				init();
				while (!stop) {
					try {
						exchangeRate = getExchangeRate(exchange,
								exchangeRateAPI);
						walletBalance = getWalletBalance(walletBalanceExchange,
								walletAddresses);
						System.gc();
						Thread.sleep(restArbitragePollingInterval);
					} catch (Throwable e) {
						log.error("Throwable: " + e.getMessage());
						e.printStackTrace();
					}
				}
			} catch (ApplicationException e) {
				log.error("ApplicationException: " + e.getMessage());
				e.printStackTrace();
			}
			client.close();
		}
	}
}
