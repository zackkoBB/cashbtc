package com.gconsulting.webapp.action;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;

import org.springframework.stereotype.Component;

import com.gconsulting.webapp.arbitrage.ArbitrageScanner;

@ViewScoped
@Component("arbitrageScannerManagerAction")
public class ArbitrageScannerManager extends BasePage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8264468867722402963L;

	public ArbitrageScannerManager() {
		super();
	}

	public boolean getArbitrageThreadIsAlive() {
		return ArbitrageScanner.getInstance().arbitrageThreadIsAlive();
	}

	public boolean getArbitrageThreadIsInterrupted() {
		return ArbitrageScanner.getInstance().arbitrageThreadIsInterrupted();
	}

	public void arbitrageThreadCheckAccess() {
		ArbitrageScanner.getInstance().arbitrageThreadCheckAccess();
	}

	public String getArbitrageThreadState() {
		return ArbitrageScanner.getInstance().arbitrageThreadState().toString();
	}

	public void arbitrageThreadRestart() {
		ArbitrageScanner.getInstance().arbitrageThreadRestart();
	}
}
