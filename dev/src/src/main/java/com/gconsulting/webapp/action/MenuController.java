package com.gconsulting.webapp.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.gconsulting.Constants;
import com.gconsulting.model.Role;
import com.gconsulting.model.User;
import com.gconsulting.service.UserManager;
import com.gconsulting.service.impl.UserManagerImpl;

@Scope("session")
// @Component("menuController")
@Controller("menuController")
public class MenuController extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7204659847632369338L;
	private String id;
	private User user = new User();

	public MenuController() {

		super();
		userManager = new UserManagerImpl();
	}

	private void getUserFromDB() {

		HttpServletRequest request = getRequest();
		// if a user's id is passed in
		if (id != null) {
			log.debug("Editing user, id is: " + id);
			// lookup the user using that id
			user = userManager.getUser(id);
		} else {
			if (request.getRemoteUser() != null) {
				user = userManager.getUserByUsername(request.getRemoteUser());
			} else {
				user = null;
			}
		}
		if (user != null) {
			if (user.getUsername() != null) {
				user.setConfirmPassword(user.getPassword());
				if (isRememberMe()) {
					// if user logged in with remember me, display a warning
					// that they can't change passwords
					log.debug("checking for remember me login...");
					log.trace("User '" + user.getUsername()
							+ "' logged in with cookie");
					addFacesMessage("userProfile.cookieLogin");
				}
			}
		}		
	}
	
	@PostConstruct
	public void init() {
		getUserFromDB();
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	public void registerUserNewsletter() {
		/*
		 * TODO: Register insterted E-Mail and display message (Optional): send
		 * confirmation email.
		 */
		addFacesMessage("website.home.newsletterOK");
	}

	public boolean getUserLoggedIn() {

		if (user != null) {
			return true;
		} else {
			return false;
		}
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTheme() {

//		if (user == null) {
//			init();
//		}
		getUserFromDB();
		return user.getTheme();
	}

	public boolean isAdmin() {

		if (user == null) {
			init();
		}
		for (Role role : user.getRoles()) {
			if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
				return true;
			}
		}
		return false;
	}
//
//	public boolean isUser() {
//
//		for (Role role : user.getRoles()) {
//			if (role.getName().equalsIgnoreCase(Constants.USER_ROLE)) {
//				return true;
//			}
//		}
//		return false;
//	}

	public String create() {

		HttpServletRequest request = getRequest();
		// if a user's id is passed in
		if (id != null) {
			log.debug("Editing user, id is: " + id);
			// lookup the user using that id
			user = userManager.getUser(id);
		} else {
			user = userManager.getUserByUsername(request.getRemoteUser());
		}
		if (user.getUsername() != null) {
			user.setConfirmPassword(user.getPassword());
			if (isRememberMe()) {
				// if user logged in with remember me, display a warning that
				// they can't change passwords
				log.debug("checking for remember me login...");
				log.trace("User '" + user.getUsername()
						+ "' logged in with cookie");
				addFacesMessage("userProfile.cookieLogin");
			}
		}
		return "home";
	}

	/**
	 * Convenience method for view templates to check if the user is logged in
	 * with RememberMe (cookies).
	 * 
	 * @return true/false - false if user interactively logged in.
	 */
	public boolean isRememberMe() {

		if (user != null && user.getId() == null)
			return false; // check for add()
		AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
		SecurityContext ctx = SecurityContextHolder.getContext();
		if (ctx != null) {
			Authentication auth = ctx.getAuthentication();
			return resolver.isRememberMe(auth);
		}
		return false;
	}
}
