package com.gconsulting.webapp.model;

public class Transaction {

	private String code;
	private Double euro;
	private Double btc;
	private Boolean fulfilled;

	public Transaction() {
		super();
	}

	public Transaction(String code, Double euro, Double btc, Boolean fulfilled) {
		super();
		this.code = code;
		this.euro = euro;
		this.btc = btc;
		this.fulfilled = fulfilled;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getEuro() {
		return euro;
	}

	public void setEuro(Double euro) {
		this.euro = euro;
	}

	public Double getBtc() {
		return btc;
	}

	public void setBtc(Double btc) {
		this.btc = btc;
	}

	public Boolean getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(Boolean fulfilled) {
		this.fulfilled = fulfilled;
	}

}
