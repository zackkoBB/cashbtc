package com.gconsulting.webapp.servlet;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gconsulting.Constants;
import com.gconsulting.service.ConfigurationManager;
import com.gconsulting.webapp.arbitrage.ArbitrageScanner;
import com.gconsulting.webapp.exception.ApplicationException;
import com.gconsulting.webapp.model.ExchangeRate;

@RestController
public class TickerController {

	protected final Log log = LogFactory.getLog(getClass());

	private final static String KEY_WRONG_MESSAGE = "Wrong Key!";
	private final static String PRICE_SELL_LABEL = "sell";
	private final static String PRICE_BUY_LABEL = "buy";
	private final static String PRICE_WRONG_MESSAGE = "No Price!";
	private final static String LANGUAGE_IT_LABEL = "IT";
	private final static String LIQUIDITY_COIN_WRONG_MESSAGE = "No Coin!";

	private ConfigurationManager configurationManager;
	private Double walletBalance;
	private Double walletBalanceEUR;
	private Double priceSellBTC;
	private Double priceBuyBTC;
	private Double feePostePay;
	private Double feeSell;

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	/**
	 * Init method called after construct
	 */
	@PostConstruct
	public void init() {

		if (configurationManager != null) {
			ArbitrageScanner.getInstance().setConfigurationManager(
					configurationManager);
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			feePostePay = new Double(configurationManager
					.getConfigurationByKey(Constants.FEE_POSTEPAY).getValue());
			feeSell = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELL).getValue());
		}
		getData();
	}

	/**
	 * Get Data from the web
	 */
	private void getData() {

		ExchangeRate exchangeRateRetrieved = ArbitrageScanner.getInstance()
				.getExchangeRate();
		if (exchangeRateRetrieved != null) {
			/*
			 * Adjust exchangeRate to our value
			 */
			priceSellBTC = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feePostePay;
			priceBuyBTC = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeSell;
		}
		if (ArbitrageScanner.getInstance().getWalletBalance() != null) {
			walletBalance = ArbitrageScanner.getInstance().getWalletBalance()
					.getRate()
					/ Constants.BTC_TO_SATOSHI;
		} else {
		}
		walletBalanceEUR = new Double(configurationManager
				.getConfigurationByKey(Constants.POSTE_PAY_BALANCE).getValue());
	}

	private void drawImage(HttpServletResponse response, String text,
			Paint paint) throws IOException {

		// response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		response.setContentType(MediaType.IMAGE_PNG_VALUE);
		ServletOutputStream out = response.getOutputStream();
		// BufferedImage image = new BufferedImage(200, 40,
		// BufferedImage.TYPE_BYTE_INDEXED);
		BufferedImage image = new BufferedImage(200, 40,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		graphics.setComposite(AlphaComposite.Clear);
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 200, 40);
		graphics.setPaint(paint);
		// Font font = new Font("Comic Sans MS", Font.PLAIN, 30);
		Font font = new Font("Verdana", Font.PLAIN, 30);
		graphics.setFont(font);
		graphics.setComposite(AlphaComposite.Src);
		graphics.drawString(text, 5, 30);
		graphics.dispose();
		ImageIO.write(image, "png", out);
		out.close();
	}

	private void returnError(HttpServletResponse response, String text)
			throws IOException {
		// GradientPaint gradientPaint = new GradientPaint(10, 5, Color.GREEN,
		// 20,
		// 10, Color.RED, true);
		drawImage(response, text, Color.RED);
	}

	// private void returnBlack(HttpServletResponse response, String text)
	// throws IOException {
	// drawImage(response, text, Color.BLACK);
	// }

	private void returnOk(HttpServletResponse response, String text)
			throws IOException {
		drawImage(response, text, Color.GREEN);
	}

	@RequestMapping(value = "/price", method = RequestMethod.GET)
	public void getPrice(
			HttpServletResponse response,
			@RequestParam(value = "type", defaultValue = PRICE_SELL_LABEL) String type,
			@RequestParam(value = "coin", defaultValue = Constants.FEE_UNIT_BTC) String coin,
			@RequestParam(value = "quantity", defaultValue = "1") Double quantity,
			@RequestParam(value = "fee", defaultValue = "5.5") Double fee,
			@RequestParam(value = "language", defaultValue = "IT") String language,
			@RequestParam(value = "key") String key)
			throws ApplicationException, IOException {

		log.info("Parameters: " + type + " " + coin + " " + quantity + " "
				+ key);
		if (key.equalsIgnoreCase(Constants.TICKER_SECRET)) {
			NumberFormat df = new DecimalFormat("#,##0.00");
			if (language.equals(LANGUAGE_IT_LABEL)) {
				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setDecimalSeparator(',');
				symbols.setGroupingSeparator('.');
				df = new DecimalFormat("#,##0.00", symbols);
			}
			feePostePay = fee;
			getData();
			if (type.equalsIgnoreCase(PRICE_SELL_LABEL)) {
				returnOk(response, "€" + df.format(priceSellBTC * quantity));
			} else if (type.equalsIgnoreCase(PRICE_BUY_LABEL)) {
				returnError(response, "€" + df.format(priceBuyBTC * quantity));
			} else {
				// No price
				returnError(response, PRICE_WRONG_MESSAGE);
			}
		} else {
			// wrong secret
			returnError(response, KEY_WRONG_MESSAGE);
		}
	}

	@RequestMapping(value = "/liquidity", method = RequestMethod.GET)
	public void getLiquidity(
			HttpServletResponse response,
			@RequestParam(value = "coin", defaultValue = Constants.FEE_UNIT_BTC) String coin,
			@RequestParam(value = "language", defaultValue = "IT") String language,
			@RequestParam(value = "key") String key)
			throws ApplicationException, IOException {

		log.info("Parameters: " + coin + " " + language + " " + key);
		if (key.equalsIgnoreCase(Constants.TICKER_SECRET)) {
			getData();
			if (coin.equalsIgnoreCase(Constants.FEE_UNIT_BTC)) {
				DecimalFormat df = new DecimalFormat("#,##0.00000000");
				if (language.equals(LANGUAGE_IT_LABEL)) {
					DecimalFormatSymbols symbols = new DecimalFormatSymbols();
					symbols.setDecimalSeparator(',');
					symbols.setGroupingSeparator('.');
					df = new DecimalFormat("#,##0.00000000", symbols);
				}
				returnOk(response, df.format(walletBalance));
			} else if (coin.equalsIgnoreCase(Constants.MARKET_EUR)) {
				DecimalFormat df = new DecimalFormat("#,##0.00");
				if (language.equals(LANGUAGE_IT_LABEL)) {
					DecimalFormatSymbols symbols = new DecimalFormatSymbols();
					symbols.setDecimalSeparator(',');
					symbols.setGroupingSeparator('.');
					df = new DecimalFormat("#,##0.00", symbols);
				}
				returnError(response, "€" + df.format(walletBalanceEUR));
			} else {
				// No coin
				returnError(response, LIQUIDITY_COIN_WRONG_MESSAGE);
			}
		} else {
			// wrong secret
			returnError(response, KEY_WRONG_MESSAGE);
		}
	}
}