package com.gconsulting.webapp.model;

import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;
import com.gconsulting.model.Market;

public class Arb {

	private Double value;
	private Market market;
	private Exchange startExchange;
	private Fee startFee;
	private Exchange endExchange;
	private Fee endFee;
	private Double startGive;
	private Double startReceive;
	private Double startAveragePrice;
	private Double endGive;
	private Double endReceive;
	private Double endAveragePrice;
	private Fee transferFee;
	private Double withdrawValue;
	private Double receivedValue;

	public Arb() {
		super();
	}

	public Arb(Double value, Market market, Exchange startExchange,
			Fee startFee, Exchange endExchange, Fee endFee, Double startGive,
			Double startReceive, Double startAveragePrice, Double endGive,
			Double endReceive, Double endAveragePrice, Fee transferFee,
			Double withdrawValue, Double receivedValue) {
		super();
		this.value = value;
		this.market = market;
		this.startExchange = startExchange;
		this.startFee = startFee;
		this.endExchange = endExchange;
		this.endFee = endFee;
		this.startGive = startGive;
		this.startReceive = startReceive;
		this.startAveragePrice = startAveragePrice;
		this.endGive = endGive;
		this.endReceive = endReceive;
		this.endAveragePrice = endAveragePrice;
		this.transferFee = transferFee;
		this.withdrawValue = withdrawValue;
		this.receivedValue = receivedValue;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public Exchange getStartExchange() {
		return startExchange;
	}

	public void setStartExchange(Exchange startExchange) {
		this.startExchange = startExchange;
	}

	public Exchange getEndExchange() {
		return endExchange;
	}

	public void setEndExchange(Exchange endExchange) {
		this.endExchange = endExchange;
	}

	public Fee getStartFee() {
		return startFee;
	}

	public void setStartFee(Fee startFee) {
		this.startFee = startFee;
	}

	public Fee getEndFee() {
		return endFee;
	}

	public void setEndFee(Fee endFee) {
		this.endFee = endFee;
	}

	public Fee getTransferFee() {
		return transferFee;
	}

	public void setTransferFee(Fee transferFee) {
		this.transferFee = transferFee;
	}

	public Double getWithdrawValue() {
		return withdrawValue;
	}

	public void setWithdrawValue(Double withdrawValue) {
		this.withdrawValue = withdrawValue;
	}

	public Double getReceivedValue() {
		return receivedValue;
	}

	public void setReceivedValue(Double receivedValue) {
		this.receivedValue = receivedValue;
	}

	public Double getStartAveragePrice() {
		return startAveragePrice;
	}

	public void setStartAveragePrice(Double startAveragePrice) {
		this.startAveragePrice = startAveragePrice;
	}

	public Double getEndAveragePrice() {
		return endAveragePrice;
	}

	public void setEndAveragePrice(Double endAveragePrice) {
		this.endAveragePrice = endAveragePrice;
	}

	public Double getStartGive() {
		return startGive;
	}

	public void setStartGive(Double startGive) {
		this.startGive = startGive;
	}

	public Double getStartReceive() {
		return startReceive;
	}

	public void setStartReceive(Double startReceive) {
		this.startReceive = startReceive;
	}

	public Double getEndGive() {
		return endGive;
	}

	public void setEndGive(Double endGive) {
		this.endGive = endGive;
	}

	public Double getEndReceive() {
		return endReceive;
	}

	public void setEndReceive(Double endReceive) {
		this.endReceive = endReceive;
	}
}
