package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.Constants;
import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;

public class KRAKENInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		JSONObject data = source.getJSONObject("pairs");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source, String market)
			throws JSONException {

		return getOrderbookByMarket(source, getMarket(market));
	}

	private String getMarket(String market) {

		if (market.startsWith(Constants.MARKET_BTC_USD)) {
			return "XXBTZUSD";
		} else if (market.startsWith(Constants.MARKET_BTC_EUR)) {
			return "XXBTZEUR";
		} else if (market.startsWith(Constants.MARKET_BTC_GBP)) {
			return "XXBTZGBP";
		}
		return null;
	}

	private Orderbook getOrderbookByMarket(JSONObject source, String marketCode)
			throws JSONException {

		Orderbook result = new Orderbook();
		JSONObject data = source.getJSONObject("result");
		JSONObject returnObject = data.getJSONObject(marketCode);
		JSONArray asks = returnObject.getJSONArray("asks");
		JSONArray bids = returnObject.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		double cumValue = 0.0;
		double price = 0.0;
		double amount = 0.0;
		for (int i = 0; i < asks.length(); i++) {
			JSONArray jsonarray = asks.getJSONArray(i);
			price = new Double((String) jsonarray.get(0));
			amount = new Double((String) jsonarray.get(1));
			cumValue += price * amount;
			sellOrders.add(new Order(price, amount, cumValue));
		}
		result.setSellOrders(sellOrders);
		cumValue = 0.0;
		price = 0.0;
		amount = 0.0;
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			price = new Double((String) jsonarray.get(0));
			amount = new Double((String) jsonarray.get(1));
			cumValue += price * amount;
			buyOrders.add(new Order(price, amount, cumValue));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}

	public ExchangeRate getTicker(JSONObject source) throws JSONException {

		ExchangeRate result = new ExchangeRate();
		JSONObject data = source.getJSONObject("result");
		JSONObject returnObject = data.getJSONObject("XXBTZEUR");
		JSONArray lastPrice = returnObject.getJSONArray("c");
		double price = new Double((String) lastPrice.get(0));
		result.setRate(price);
		result.setTime(Calendar.getInstance().getTimeInMillis() / 1000);
		return result;
	}
}
