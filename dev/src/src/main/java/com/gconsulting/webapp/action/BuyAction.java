package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.Transaction;
import com.gconsulting.service.ConfigurationManager;
import com.gconsulting.service.MailEngine;
import com.gconsulting.service.TransactionManager;
import com.gconsulting.webapp.arbitrage.ArbitrageScanner;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.ExchangeRateView;
import com.gconsulting.webapp.util.RESTUtils;

/**
 * Managed Bean to send password hints to registered users.
 *
 * <p>
 * <a href="PasswordHint.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Scope("session")
// @ViewScoped
@Component("buyAction")
// @ManagedBean
public class BuyAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1317756065202142811L;
	private boolean walletEmpty;
	private boolean confirmationCodeEmpty;
	private Double walletBalance;
	private Double walletBalanceEur;
	private List<Paysafecard> paysafecards;
	private String code;
	private String confirmationCode;
	private String sizePostePayString; // sizePostePayString.replace("฿", "")
	private Integer sizePostePay;
	private Double totalSizePostePay;
	private Double totalSize;
	private String address;
	private List<ExchangeRateView> sizes;
	private TransactionManager transactionManager;
	private ConfigurationManager configurationManager;
	private Double pricePaysafecard;
	private Double pricePostePay;
	private Double feePaysafecard;
	private Double feePostePay;
	private Double priceSell;
	private Double feeSell;
	private String email;
	private String orderSubmittedSummary;
	private String orderSubmittedDetail;
	private List<Paysafecard> lastPaysafecards;
	private String lastAddress;
	private String lastEmail;
	private String lastCode;
	private String sizePostePayEUR;
	private String sizePostePayBTC;
	private String contactEmail;
	private String contactSubject;
	private String contactText;
	private String orderStatusAddress;
	private List<com.gconsulting.webapp.model.Transaction> orderHistory;
	private Integer contactFormActiveIndex;
	private Integer orderStatusFormActiveIndex;

	/**
	 * Constants
	 */
	private Integer pollingInterval;
	private String postePayName;
	private String postePayNumber;
	private String postePaySSN;

	/**
	 * Default constructor
	 */
	public BuyAction() {
		super();
	}

	/**
	 * Init method called after construct
	 */
	@PostConstruct
	public void init() {

		if (configurationManager != null) {
			ArbitrageScanner.getInstance().setConfigurationManager(
					configurationManager);
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			pollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
					.getValue());
			postePayName = configurationManager.getConfigurationByKey(
					Constants.POSTE_PAY_NAME).getValue();
			postePayNumber = configurationManager.getConfigurationByKey(
					Constants.POSTE_PAY_NUMBER).getValue();
			postePaySSN = configurationManager.getConfigurationByKey(
					Constants.POSTE_PAY_SSN).getValue();
			feePaysafecard = new Double(configurationManager
					.getConfigurationByKey(Constants.FEE_PAYSAFECARD)
					.getValue());
			feePostePay = new Double(configurationManager
					.getConfigurationByKey(Constants.FEE_POSTEPAY).getValue());
			feeSell = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELL).getValue());
		}
		paysafecards = new ArrayList<>();
		paysafecards.add(new Paysafecard("", "", "", "", 0.0));
		walletEmpty = true;
		confirmationCodeEmpty = true;
		confirmationCode = new String("");
		contactFormActiveIndex = 0;
		orderStatusFormActiveIndex = -1;
		getData();
		if (sizes != null) {
			if (sizes.size() > 0) {
				paysafecards.get(0).setSize(sizes.get(0).getValue());
			}
		}
	}

	@Autowired
	public void setTransactionManager(
			@Qualifier("orderTransactionManager") TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setMailEngine(@Qualifier("mailEngine") MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	/**
	 * Add an E-Mail for receipt
	 * 
	 * @return String label of the next view
	 */
	public String addEmail() {

		for (Paysafecard paysafecard : lastPaysafecards) {
			Transaction transaction = transactionManager
					.getTransactionByCode(paysafecard.getPIN());
			transaction.setNote(email);
			transactionManager.update(transaction);
		}
		return "receipt";
	}

	/**
	 * Send an email Memo
	 * 
	 * @param PINs
	 *            of the transaction
	 * @param size
	 *            total size of the transaction
	 */
	private void sendMailPostePay(String subject, String destination) {

		message = new SimpleMailMessage();
		message.setFrom("contatti@cashbtc.it");
		message.setTo(destination);
		message.setSubject(subject);
		StringBuilder msg = new StringBuilder();
		// msg.append("<b>" + getText("success.orderSubmitted.orderSummary") +
		// "</b>"
		// + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.orderSummary")
				+ System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.orderSummary.confirmationCode")
				+ ": " + lastCode + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.orderSummary.bitcoin")
				+ ": " + sizePostePayBTC + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.orderSummary.address")
				+ ": " + lastAddress + System.getProperty("line.separator")
				+ System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.postePayTopUpDetails")
				+ System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.postePayTopUpDetails.euro")
				+ ": " + sizePostePayEUR + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.postePayTopUpDetails.number")
				+ ": " + postePayNumber + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.postePayTopUpDetails.name")
				+ ": " + postePayName + System.getProperty("line.separator"));
		msg.append(getText("success.orderSubmitted.postePayTopUpDetails.ssn")
				+ ": " + postePaySSN + System.getProperty("line.separator"));
		message.setText(msg.toString());
		mailEngine.send(message);
	}

	/**
	 * Send contact email
	 * 
	 * @return
	 */
	public String sendContactEmail() {

		try {
			message = new SimpleMailMessage();
			message.setFrom("contatti@cashbtc.it");
			message.setTo("contatti@cashbtc.it");
			String subject = '[' + getText("webapp.name") + "] "
					+ contactSubject;
			message.setSubject(subject);
			StringBuilder msg = new StringBuilder();
			msg.append("Email: " + contactEmail
					+ System.getProperty("line.separator"));
			msg.append(contactText);
			message.setText(msg.toString());
			mailEngine.send(message);
			addFacesMessage("website.contact.contactUs.success");
			contactEmail = new String();
			contactSubject = new String();
			contactText = new String();
		} catch (Exception e) {
			addFacesError("website.contact.contactUs.error", e.getMessage());
		}
		return "contact";
	}

	/**
	 * Search Order
	 * 
	 * @return
	 */
	public String searchOrder() {

		orderHistory = new ArrayList<com.gconsulting.webapp.model.Transaction>();
		List<Transaction> transactions = transactionManager
				.getTransactionByAddress(orderStatusAddress);
		if (transactions != null) {
			if (transactions.size() > 0) {
				for (Transaction transaction : transactions) {
					Double euro = new Double(transaction.getNote().substring(
							transaction.getNote().indexOf("€") + 1,
							transaction.getNote().indexOf(" ")));
					orderHistory
							.add(new com.gconsulting.webapp.model.Transaction(
									transaction.getCode(), euro, transaction
											.getAmount(), transaction
											.getFulfilled()));
				}
				contactFormActiveIndex = 1;
				orderStatusFormActiveIndex = 0;
			} else {
				contactFormActiveIndex = 1;
				orderStatusFormActiveIndex = -1;
				addFacesError("errors.address.notFound");
			}
		}
		return "contact";
	}

	public void contactFormOnTabChange() {

		if (contactFormActiveIndex == 0) {
			contactFormActiveIndex = 1;
			orderStatusFormActiveIndex = -1;
		} else if (contactFormActiveIndex == 1) {
			contactFormActiveIndex = 0;
			orderStatusFormActiveIndex = -1;
			orderHistory = new ArrayList<com.gconsulting.webapp.model.Transaction>();
			orderStatusAddress = new String();
		}
	}

	/**
	 * Add an E-Mail for memo
	 * 
	 * @return String label of the next view
	 */
	public String addEmailPostePay() {

		Transaction transaction = transactionManager.getTransactionByCode(code);
		transaction.setNote(transaction.getNote() + " " + email);
		transactionManager.update(transaction);
		lastEmail = email;
		String subject = '[' + getText("webapp.name") + "] "
				+ getText("website.home.newOrder");
		sendMailPostePay(subject, email);
		return "memo";
	}

	/**
	 * Add one Paysafecard PIN to the view
	 * 
	 * @return String label of the next view
	 */
	public String addPaysafecard() {

		if (paysafecards.size() < Constants.PAYSAFECARD_MAX_PIN) {
			paysafecards.add(new Paysafecard("", "", "", "", sizes.get(0)
					.getValue()));
		}
		return "add";
	}

	/**
	 * Remove one Paysafecard PIN to the view
	 * 
	 * @return String label of the next view
	 */
	public String removePaysafecard() {

		if (paysafecards.size() > 1) {
			paysafecards.remove(paysafecards.size() - 1);
		}
		return "remove";
	}

	/**
	 * Send an email
	 * 
	 * @param PINs
	 *            of the transaction
	 * @param size
	 *            total size of the transaction
	 */
	private void sendMail(String subject, String PINs, Double size) {

		message = new SimpleMailMessage();
		message.setFrom("contatti@cashbtc.it");
		message.setTo(new String[] { "gsit80@gmail.com",
				"393402128138@bulksms.net" });
		message.setSubject(subject + " Forward");
		StringBuilder msgSMS = new StringBuilder();
		msgSMS.append("PIN: " + PINs + System.getProperty("line.separator"));
		msgSMS.append("BTC: " + new DecimalFormat("#.########").format(size)
				+ System.getProperty("line.separator"));
		msgSMS.append("Address: " + address
				+ System.getProperty("line.separator"));
		message.setText(msgSMS.toString());
		mailEngine.send(message);

	}

	/**
	 * Check for a valid Paysafecard transaction
	 * 
	 * @return boolean true if is a valid Paysafecard transaction
	 */
	private boolean validPaysafecardTransaction() {

		/*
		 * Check for duplicate PIN
		 */
		totalSize = 0.0;
		for (Paysafecard paysafecard : paysafecards) {
			Transaction transaction = transactionManager
					.getTransactionByCode(paysafecard.getPIN());
			if (transaction != null) {
				addFacesError("errors.existing.transaction",
						paysafecard.getPINSeparated());
				return false;
			}
			totalSize += paysafecard.getSize();
		}
		/*
		 * Check BTC address
		 */
		String walletBalanceAPI = configurationManager.getConfigurationByKey(
				Constants.WALLET_BALANCE_API).getValue();
		Response response = RESTUtils.getInstance().getAPI(
				walletBalanceAPI + address);
		if (response.getStatus() != 200) {
			addFacesError("errors.validAddress", address);
			return false;
		}
		/*
		 * Check BTC wallet balance
		 */
		if (ArbitrageScanner.getInstance().getWalletBalance() != null) {
			walletBalance = ArbitrageScanner.getInstance().getWalletBalance()
					.getRate()
					/ Constants.BTC_TO_SATOSHI;
			if (walletBalance < totalSize) {
				addFacesError("errors.notEnough", "BTCs");
				return false;
			}
			// walletBalanceEur = walletBalance * pricePostePay;
			// defaultSizePostePay = 5.0;
			// totalSizePostePay = defaultSizePostePay / pricePostePay;
		} else {
			addFacesError("errors.token");
			return false;
		}
		/*
		 * Check distinct PINs
		 */
		if (paysafecards.size() > 1) {
			for (int i = 0; i < paysafecards.size(); i++) {
				for (int j = 0; j < paysafecards.size(); j++) {
					if (i != j) {
						if (paysafecards.get(i).getPIN()
								.equalsIgnoreCase(paysafecards.get(j).getPIN())) {
							addFacesError("errors.distinct");
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Create a Paysafecard transaction
	 * 
	 * @return boolean true if is a valid Paysafecard transaction
	 */
	private boolean createPaysafecardTransaction() {

		try {
			/*
			 * Create transactions
			 */
			String successMessageDetail = new String("");
			if (paysafecards.size() > 5) {
				/*
				 * Send 2 messages
				 */
				for (int i = 0; i < 5; i++) {
					transactionManager.create(new Transaction(paysafecards.get(
							i).getPIN(), address,
							paysafecards.get(i).getSize(), Calendar
									.getInstance().getTimeInMillis() / 1000,
							"", new Boolean(false)));
					successMessageDetail = successMessageDetail
							.concat(paysafecards.get(i).getPINSeparated() + " ");
				}
				successMessageDetail = successMessageDetail.concat("cont. ");
				String subject = '[' + getText("webapp.name") + "] "
						+ getText("website.home.newOrder") + " 1/2";
				sendMail(subject, successMessageDetail, totalSize);
				successMessageDetail = new String("");
				for (int i = 5; i < paysafecards.size(); i++) {
					transactionManager.create(new Transaction(paysafecards.get(
							i).getPIN(), address,
							paysafecards.get(i).getSize(), Calendar
									.getInstance().getTimeInMillis() / 1000,
							"", new Boolean(false)));
					successMessageDetail = successMessageDetail
							.concat(paysafecards.get(i).getPINSeparated() + " ");
				}
				subject = '[' + getText("webapp.name") + "] "
						+ getText("website.home.newOrder") + " 2/2";
				sendMail(subject, successMessageDetail, totalSize);
			} else {
				/*
				 * Send 1 message
				 */
				for (Paysafecard paysafecard : paysafecards) {
					transactionManager.create(new Transaction(paysafecard
							.getPIN(), address, paysafecard.getSize(), Calendar
							.getInstance().getTimeInMillis() / 1000, "",
							new Boolean(false)));
					successMessageDetail = successMessageDetail
							.concat(paysafecard.getPINSeparated() + " ");
				}
				String subject = '[' + getText("webapp.name") + "] "
						+ getText("website.home.newOrder");
				sendMail(subject, successMessageDetail, totalSize);
			}
			String successMessageSummary = getText(
					"success.orderSubmittedSummary", new DecimalFormat(
							"#.########").format(totalSize));
			String linkToPaysafecard = new String("<a href="
					+ getRequest().getContextPath() + "/about#5fifth>"
					+ getText("website.about.faq.5fifth") + "</a>");
			orderSubmittedSummary = successMessageSummary;
			orderSubmittedDetail = getText("success.orderSubmittedDetail",
					new Object[] { successMessageDetail, linkToPaysafecard });
			lastPaysafecards = new ArrayList<>();
			for (Paysafecard paysafecard : paysafecards) {
				lastPaysafecards.add(paysafecard);
			}
		} catch (MailException me) {
			addFacesError("errors.detail", me.getMessage());
			log.error("MailException: " + me.getMessage());
			me.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Buy action
	 * 
	 * @return String label of the next view
	 */
	public String cancel() {
		reset();
		return "cancel";
	}

	/**
	 * Reset everything after a successfull buy
	 */
	private void reset() {

		paysafecards = new ArrayList<>();
		paysafecards.add(new Paysafecard("", "", "", "", 0.0));
		address = new String();
		email = new String();
		confirmationCodeEmpty = true;
		confirmationCode = new String("");
		getData();
	}

	/**
	 * Buy action
	 * 
	 * @return String label of the next view
	 */
	public String buyPaysafecard() {

		if (validPaysafecardTransaction()) {
			if (createPaysafecardTransaction()) {
				reset();
				return "success";
			}
		}
		return "error";
	}

	/**
	 * Check for a valid PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 */
	private boolean validPostePayTransaction() {

		/*
		 * Check input data
		 */
		if (code != null) {
			// log.info("Code: " + code);
		} else {
			addFacesError("errors.token");
			return false;
		}
		if (sizePostePayString != null) {
			String sizeTemp = sizePostePayString.substring(sizePostePayString
					.indexOf("฿") + 1);
			totalSize = new Double(sizeTemp);
			// log.info("SizeString: " + sizePostePayString);
			// log.info("Size: " + totalSize);
		} else {
			addFacesError("errors.token");
			return false;
		}
		if (address != null) {
			// log.info("Address: " + address);
		} else {
			addFacesError("errors.token");
			return false;
		}
		/*
		 * Check for duplicate Transaction: the code should be already OK.
		 */
		/*
		 * Check BTC address
		 */
		String walletBalanceAPI = configurationManager.getConfigurationByKey(
				Constants.WALLET_BALANCE_API).getValue();
		Response response = RESTUtils.getInstance().getAPI(
				walletBalanceAPI + address);
		if (response.getStatus() != 200) {
			addFacesError("errors.validAddress", address);
			return false;
		}
		/*
		 * Check BTC wallet balance
		 */
		if (ArbitrageScanner.getInstance().getWalletBalance() != null) {
			walletBalance = ArbitrageScanner.getInstance().getWalletBalance()
					.getRate()
					/ Constants.BTC_TO_SATOSHI;
			if (walletBalance < totalSize) {
				addFacesError("errors.notEnough", "BTCs");
				return false;
			}
			// walletBalanceEur = walletBalance * pricePostePay;
			// defaultSizePostePay = 5.0;
			// totalSizePostePay = defaultSizePostePay / pricePostePay;
		} else {
			addFacesError("errors.token");
			return false;
		}
		/*
		 * Check distinct PINs: not needed
		 */
		return true;
	}

	/**
	 * Create a PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 */
	private boolean createPostePayTransaction() {

		try {
			/*
			 * Create transactions
			 */
			transactionManager.create(new Transaction(code, address, totalSize,
					Calendar.getInstance().getTimeInMillis() / 1000,
					sizePostePayString.replace("฿", ""), new Boolean(false)));
			// String subject = '[' + getText("webapp.name") + "] "
			// + getText("website.home.newOrder");
			// sendMail(subject, successMessageDetail, totalSize);
			// String successMessageDetail = new String("");
			String successMessageSummary = getText(
					"success.orderSubmittedPostePaySummary", new DecimalFormat(
							"#.########").format(totalSize));
			// String linkToPaysafecard = new String("<a href="
			// + getRequest().getContextPath() + "/about#5fifth>"
			// + getText("website.about.faq.5fifth") + "</a>");
			orderSubmittedSummary = successMessageSummary;
			lastAddress = address;
			lastCode = code;
			sizePostePayBTC = "฿" + totalSize.toString();
			sizePostePayEUR = sizePostePayString.substring(
					sizePostePayString.indexOf("€"),
					sizePostePayString.indexOf("/") - 1);
			// orderSubmittedDetail = getText("success.orderSubmittedDetail",
			// new Object[] { successMessageDetail, linkToPaysafecard });
		} catch (MailException me) {
			addFacesError("errors.detail", me.getMessage());
			log.error("MailException: " + me.getMessage());
			me.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Buy action PostePay
	 * 
	 * @return String label of the next view
	 */
	public void setSizeParameter() {

		FacesContext context = FacesContext.getCurrentInstance();
		Map<?, ?> map = context.getExternalContext().getRequestParameterMap();
		String sizeParameter = (String) map.get("sizeParameter");
		sizePostePayString = sizeParameter;
	}

	/**
	 * Buy action PostePay
	 * 
	 * @return String label of the next view
	 */
	public String buyPostePay() {

		if (validPostePayTransaction()) {
			if (createPostePayTransaction()) {
				reset();
				return "successPostePay";
			}
		}
		return "error";
	}

	public void uploadConfirmationCodeEnabler() {

		// log.info("confirmationCode: " + confirmationCode);
		if (confirmationCode != null) {
			if (confirmationCode.length() > 0) {
				confirmationCodeEmpty = false;
				return;
			}
		}
		confirmationCodeEmpty = true;
	}

	public String uploadConfirmationCode() {

		if (confirmationCode != null) {
			if (confirmationCode.length() > 0) {
				Transaction transaction = transactionManager
						.getTransactionByCode(confirmationCode);
				if (transaction != null) {
					if (!transaction.getFulfilled()) {
						transaction.setFulfilled(new Boolean(true));
						transactionManager.update(transaction);
						addFacesMessage("success.confirmationCode.success",
								new DecimalFormat("#.########")
										.format(transaction.getAmount()));
					} else {
						// error
						addFacesError("success.confirmationCode.error",
								confirmationCode);
					}
				} else {
					// error
					addFacesError("success.confirmationCode.error",
							confirmationCode);
				}
			} else {
				// error
				addFacesError("success.confirmationCode.error",
						confirmationCode);
			}
		} else {
			// error
			addFacesError("success.confirmationCode.error", confirmationCode);
		}
		reset();
		return "confirm";
	}

	/**
	 * Get Data from the web
	 */
	private void getData() {

		sizes = new ArrayList<>();
		ExchangeRate exchangeRateRetrieved = ArbitrageScanner.getInstance()
				.getExchangeRate();
		if (exchangeRateRetrieved != null) {
			/*
			 * Adjust exchangeRate to our value
			 */
			pricePaysafecard = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feePaysafecard;
			pricePostePay = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feePostePay;
			priceSell = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeSell;
			sizes.add(new ExchangeRateView(Constants.FEE_UNIT_EUR + "10",
					new Double(10.0 / pricePaysafecard), ""));
			sizes.add(new ExchangeRateView(Constants.FEE_UNIT_EUR + "25",
					new Double(25.0 / pricePaysafecard), ""));
			sizes.add(new ExchangeRateView(Constants.FEE_UNIT_EUR + "50",
					new Double(50.0 / pricePaysafecard), ""));
			sizes.add(new ExchangeRateView(Constants.FEE_UNIT_EUR + "100",
					new Double(100.0 / pricePaysafecard), ""));
			/*
			 * Check if it's the first call
			 */
			if (paysafecards.get(0).getSize() == 0.0) {
				paysafecards.get(0).setSize(sizes.get(0).getValue());
			}
		}
		if (ArbitrageScanner.getInstance().getWalletBalance() != null) {
			walletBalance = ArbitrageScanner.getInstance().getWalletBalance()
					.getRate()
					/ Constants.BTC_TO_SATOSHI;
			if (walletBalance < Constants.WALLET_BALANCE_MINIMUM_AMOUNT) {
				walletEmpty = true;
			} else {
				walletEmpty = false;
			}
			walletBalanceEur = walletBalance * pricePostePay;
			sizePostePay = (new Double(walletBalanceEur * 3 / 4)).intValue();
			totalSizePostePay = (new Double(sizePostePay)) / pricePostePay;
			/*
			 * TODO: search locale No cause needs to be implemented also on
			 * client side
			 */
			DecimalFormat df = new DecimalFormat("#,##0.00");
			DecimalFormat df1 = new DecimalFormat("#,##0.00000000");
			sizePostePayString = "€" + df.format(sizePostePay) + " / ฿"
					+ df1.format(totalSizePostePay);
			// if(getRequest().getLocale().equals(Locale.UK)) {
			// DecimalFormat df = new DecimalFormat("#,##0.00");
			// DecimalFormat df1 = new DecimalFormat("#,##0.00000000");
			// sizePostePayString = "€" + df.format(sizePostePay) + " / ฿"
			// + df1.format(totalSizePostePay);
			// } else {
			// DecimalFormatSymbols dfs =
			// DecimalFormatSymbols.getInstance(Locale.ITALIAN);
			// dfs.setDecimalSeparator(',');
			// dfs.setGroupingSeparator('.');
			// DecimalFormat df = new DecimalFormat("#,##0.00",dfs);
			// DecimalFormat df1 = new DecimalFormat("#,##0.00000000",dfs);
			// sizePostePayString = "€" + df.format(sizePostePay) + " / ฿"
			// + df1.format(totalSizePostePay);
			// }
		} else {
			walletEmpty = true;
		}
	}

	/**
	 * Update view
	 */
	public void updateForm() {
		getData();
	}

	public boolean getWalletEmpty() {
		return walletEmpty;
	}

	public List<ExchangeRateView> getSizes() {
		return sizes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getPricePaysafecard() {
		return pricePaysafecard;
	}

	public void setPricePaysafecard(Double pricePaysafecard) {
		this.pricePaysafecard = pricePaysafecard;
	}

	public Double getFeePaysafecard() {
		return feePaysafecard;
	}

	public void setFeePaysafecard(Double feePaysafecard) {
		this.feePaysafecard = feePaysafecard;
	}

	public Double getPricePostePay() {
		return pricePostePay;
	}

	public void setPricePostePay(Double pricePostePay) {
		this.pricePostePay = pricePostePay;
	}

	public Double getFeePostePay() {
		return feePostePay;
	}

	public void setFeePostePay(Double feePostePay) {
		this.feePostePay = feePostePay;
	}

	public Double getPriceSell() {
		return priceSell;
	}

	public void setPriceSell(Double priceSell) {
		this.priceSell = priceSell;
	}

	public Double getFeeSell() {
		return feeSell;
	}

	public void setFeeSell(Double feeSell) {
		this.feeSell = feeSell;
	}

	public List<Paysafecard> getPaysafecards() {
		return paysafecards;
	}

	public void setPaysafecards(List<Paysafecard> paysafecards) {
		this.paysafecards = paysafecards;
	}

	public Double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(Double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public Double getWalletBalanceEur() {
		return walletBalanceEur;
	}

	public void setWalletBalanceEur(Double walletBalanceEur) {
		this.walletBalanceEur = walletBalanceEur;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderSubmittedSummary() {
		return orderSubmittedSummary;
	}

	public void setOrderSubmittedSummary(String orderSubmittedSummary) {
		this.orderSubmittedSummary = orderSubmittedSummary;
	}

	public String getOrderSubmittedDetail() {
		return orderSubmittedDetail;
	}

	public void setOrderSubmittedDetail(String orderSubmittedDetail) {
		this.orderSubmittedDetail = orderSubmittedDetail;
	}

	public Integer getSizePostePay() {
		return sizePostePay;
	}

	public void setSizePostePay(Integer sizePostePay) {
		this.sizePostePay = sizePostePay;
	}

	public Double getTotalSizePostePay() {
		return totalSizePostePay;
	}

	public void setTotalSizePostePay(Double totalSizePostePay) {
		this.totalSizePostePay = totalSizePostePay;
	}

	public String getSizePostePayString() {
		return sizePostePayString;
	}

	public void setSizePostePayString(String sizePostePayString) {
		this.sizePostePayString = sizePostePayString;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getPostePayName() {
		return postePayName;
	}

	public void setPostePayName(String postePayName) {
		this.postePayName = postePayName;
	}

	public String getPostePayNumber() {
		return postePayNumber;
	}

	public void setPostePayNumber(String postePayNumber) {
		this.postePayNumber = postePayNumber;
	}

	public String getPostePaySSN() {
		return postePaySSN;
	}

	public void setPostePaySSN(String postePaySSN) {
		this.postePaySSN = postePaySSN;
	}

	public String getLastAddress() {
		return lastAddress;
	}

	public void setLastAddress(String lastAddress) {
		this.lastAddress = lastAddress;
	}

	public String getSizePostePayEUR() {
		return sizePostePayEUR;
	}

	public void setSizePostePayEUR(String sizePostePayEUR) {
		this.sizePostePayEUR = sizePostePayEUR;
	}

	public String getSizePostePayBTC() {
		return sizePostePayBTC;
	}

	public void setSizePostePayBTC(String sizePostePayBTC) {
		this.sizePostePayBTC = sizePostePayBTC;
	}

	public String getLastEmail() {
		return lastEmail;
	}

	public void setLastEmail(String lastEmail) {
		this.lastEmail = lastEmail;
	}

	public String getLastCode() {
		return lastCode;
	}

	public void setLastCode(String lastCode) {
		this.lastCode = lastCode;
	}

	public boolean isConfirmationCodeEmpty() {
		return confirmationCodeEmpty;
	}

	public void setConfirmationCodeEmpty(boolean confirmationCodeEmpty) {
		this.confirmationCodeEmpty = confirmationCodeEmpty;
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactSubject() {
		return contactSubject;
	}

	public void setContactSubject(String contactSubject) {
		this.contactSubject = contactSubject;
	}

	public String getContactText() {
		return contactText;
	}

	public void setContactText(String contactText) {
		this.contactText = contactText;
	}

	public String getOrderStatusAddress() {
		return orderStatusAddress;
	}

	public void setOrderStatusAddress(String orderStatusAddress) {
		this.orderStatusAddress = orderStatusAddress;
	}

	public List<com.gconsulting.webapp.model.Transaction> getOrderHistory() {
		return orderHistory;
	}

	public void setOrderHistory(
			List<com.gconsulting.webapp.model.Transaction> orderHistory) {
		this.orderHistory = orderHistory;
	}

	public Integer getContactFormActiveIndex() {
		return contactFormActiveIndex;
	}

	public void setContactFormActiveIndex(Integer contactFormActiveIndex) {
		this.contactFormActiveIndex = contactFormActiveIndex;
	}

	public Integer getOrderStatusFormActiveIndex() {
		return orderStatusFormActiveIndex;
	}

	public void setOrderStatusFormActiveIndex(Integer orderStatusFormActiveIndex) {
		this.orderStatusFormActiveIndex = orderStatusFormActiveIndex;
	}

	public String getTransactionCode() {

		code = new String("");
		for (;;) {
			code = RandomStringUtils.randomNumeric(4);
			if (transactionManager.getTransactionByCode(code) == null) {
				return code;
			}
		}
	}
}
