package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.User;
import com.gconsulting.service.RoleManager;
import com.gconsulting.service.UserExistsException;

/**
 * JSF Page class to handle signing up a new user.
 *
 * @author mraible
 */
@Scope("request")
@Component("registerForm")
public class RegisterForm extends BasePage implements Serializable {

	private static final long serialVersionUID = 3524937486662786265L;
	private User user = new User();
	private RoleManager roleManager;

	public String[] getThemes() {
		return (Constants.POSSIBLE_THEMES);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Autowired
	public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public String save() throws Exception {

		user.setEnabled(true);
		// Set the default user role on this new user
		user.addRole(roleManager.getRole(Constants.USER_ROLE));
		//check theme
		if(user.getTheme().equalsIgnoreCase("default")) {
			user.setTheme(Constants.DEFAULT_THEME);
		}
		try {
			user = userManager.saveUser(user);
		} catch (AccessDeniedException ade) {
			// thrown by UserSecurityAdvice configured in aop:advisor
			// userManagerSecurity
			log.warn(ade.getMessage());
			getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		} catch (UserExistsException e) {
			addFacesError("errors.existing.user",
					new Object[] { user.getUsername(), user.getEmail() });
			// redisplay the unencrypted passwords
			user.setPassword(user.getConfirmPassword());
			return null;
		}
		addFacesMessage("user.registered");
		getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE);
		// log user in automatically
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getConfirmPassword(),
				user.getAuthorities());
		auth.setDetails(user);
		SecurityContextHolder.getContext().setAuthentication(auth);
		// Send an account information e-mail
		/*
		 * TODO: fix this
		 */
//		message.setSubject(getText("register.email.subject"));
//		try {
//			sendUserMessage(user, getText("register.email.message"),
//					RequestUtil.getAppURL(getRequest()));
//		} catch (MailException me) {
//			addFacesError(me.getMostSpecificCause().getMessage());
//			return null;
//		}
		return "home";
	}

	public List<String> getCurrencies() {
		return Constants.getCurrencies();
	}
}
