package com.gconsulting.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.model.Configuration;

/**
 * Configuration Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface ConfigurationDao extends GenericDao<Configuration, String> {

    /**
     * Gets Configuration information based on code.
     * 
     * @param Code code of the Configuration to be retrieved
     * @return Configuration retrieved
     * 
     * @throws 
     */
    @Transactional
    Configuration getConfigurationByKey(String key);

    /**
     * Gets all Configuration entities in the db
     * 
     * @return List<Configuration> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Configuration> getAll();

    /**
     * Create a new Configuration
     * 
     * @throws 
     */
    @Transactional
    void create(Configuration configuration);

    /**
     * Update an existing Configuration instance
     * 
     * @throws 
     */
    @Transactional
    void update(Configuration configuration);

    /**
     * Delete an existing Configuration instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Configuration configuration);
}
