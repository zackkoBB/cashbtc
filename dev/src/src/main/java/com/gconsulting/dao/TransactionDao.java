package com.gconsulting.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.model.Transaction;

/**
 * Transaction Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface TransactionDao extends GenericDao<Transaction, String> {

    /**
     * Gets Transaction information based on code.
     * 
     * @param Code code of the Transaction to be retrieved
     * @return Transaction retrieved
     * 
     * @throws 
     */
    @Transactional
    Transaction getTransactionByCode(String code);

	/**
	 * Gets Transaction information based on address.
	 * 
	 * @param address
	 *            address of the Transaction to be retrieved
	 * @return List<Transaction> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionByAddress(String address);

    /**
     * Gets all Transaction entities in the db
     * 
     * @return List<Transaction> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Transaction> getAll();

    /**
     * Create a new Transaction
     * 
     * @throws 
     */
    @Transactional
    void create(Transaction transaction);

    /**
     * Update an existing Transaction instance
     * 
     * @throws 
     */
    @Transactional
    void update(Transaction transaction);

    /**
     * Delete an existing Transaction instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Transaction transaction);
}
