package com.gconsulting.dao.hibernate;

import java.util.List;

import javax.persistence.Table;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;

import com.gconsulting.dao.NewsletterDao;
import com.gconsulting.model.Newsletter;
import com.gconsulting.model.User;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("newsletterDao")
public class NewsletterDaoHibernate extends GenericDaoHibernate<Newsletter, String> implements
	NewsletterDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public NewsletterDaoHibernate() {
		super(Newsletter.class);
	}

	/**
	 * Overridden simply to call the saveUser method. This is happening because
	 * saveUser flushes the session and saveObject of BaseDaoHibernate does not.
	 *
	 * @param user
	 *            the user to save
	 * @return the modified user (with a primary key set if they're new)
	 */
	@Override
	public Newsletter save(Newsletter newsletter) {
		return this.saveEmail(newsletter);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getUserPassword(Long userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(
				SessionFactoryUtils.getDataSource(getSessionFactory()));
		Table table = AnnotationUtils.findAnnotation(User.class, Table.class);
		return jdbcTemplate.queryForObject(
				"select password from " + table.name() + " where id=?",
				String.class, userId);
	}

	/**
	 * {@inheritDoc}
	 */
	public Newsletter loadNewsletterByEmail(String email) {

		List<?> newsletters = getSession().createCriteria(Newsletter.class)
				.add(Restrictions.eq("email", email)).list();
		if (newsletters == null || newsletters.isEmpty()) {
			return null;
		} else {
			return (Newsletter) newsletters.get(0);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<Newsletter> getNewsletters() {
		Query qry = getSession().createQuery(
				"from Newsletter n order by upper(n.email)");
		return qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public Newsletter saveEmail(Newsletter newsletter) {
		
		if (log.isDebugEnabled()) {
			log.debug("newsletter's id: " + newsletter.getEmail());
		}
		getSession().saveOrUpdate(newsletter);
		// necessary to throw a DataIntegrityViolation and catch it in
		// UserManager
		getSession().flush();
		return newsletter;
	}
}
