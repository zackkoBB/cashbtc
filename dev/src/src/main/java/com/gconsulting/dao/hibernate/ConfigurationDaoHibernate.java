package com.gconsulting.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.gconsulting.dao.ConfigurationDao;
import com.gconsulting.model.Configuration;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("configurationDao")
public class ConfigurationDaoHibernate extends GenericDaoHibernate<Configuration, String> implements
	ConfigurationDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public ConfigurationDaoHibernate() {
		super(Configuration.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Configuration getConfigurationByKey(String key) {
		
		Query qry = getSession().createQuery(
				"from Configuration c where c.key='" + key + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Configuration) qry.list().get(0);
			}
		}
		return null;		
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Configuration configuration) {
		getSession().save(configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Configuration configuration) {
		getSession().update(configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Configuration transaction) {
		getSession().delete(transaction);
	}

}
