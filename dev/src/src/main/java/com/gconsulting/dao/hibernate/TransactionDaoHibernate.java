package com.gconsulting.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.gconsulting.dao.TransactionDao;
import com.gconsulting.model.Transaction;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("transactionDao")
public class TransactionDaoHibernate extends GenericDaoHibernate<Transaction, String> implements
	TransactionDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public TransactionDaoHibernate() {
		super(Transaction.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByCode(String code) {
		
		Query qry = getSession().createQuery(
				"from Transaction t where t.code='" + code + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Transaction) qry.list().get(0);
			}
		}
		return null;		
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAddress(String address) {

		return (List<Transaction>) getSession().createQuery(
				"from Transaction t where t.address='" + address + "'").list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Transaction transaction) {
		getSession().save(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Transaction transaction) {
		getSession().update(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Transaction transaction) {
		getSession().delete(transaction);
	}

}
