package com.gconsulting.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gconsulting.model.Newsletter;

/**
 * User Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public interface NewsletterDao extends GenericDao<Newsletter, String> {

    /**
     * Gets users information based on login name.
     * @param username the user's username
     * @return userDetails populated userDetails object
     * @throws org.springframework.security.core.userdetails.UsernameNotFoundException thrown when user not
     * found in database
     */
    @Transactional
    Newsletter loadNewsletterByEmail(String email);

    /**
     * Gets a list of users ordered by the uppercase version of their username.
     *
     * @return List populated list of users
     */
    List<Newsletter> getNewsletters();

    /**
     * Saves a user's information.
     * @param user the object to be saved
     * @return the persisted User object
     */
    Newsletter saveEmail(Newsletter newsletter);
}
