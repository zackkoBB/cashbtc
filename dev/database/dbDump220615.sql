-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cash_btc
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `key` varchar(45) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 KEY_BLOCK_SIZE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES ('ENVIRONMENT','PROD'),('EXCHANGE_RATE_API','https://api.kraken.com/0/public/Ticker?pair=XXBTZEUR'),('EXCHANGE_RATE_EXCHANGE','KRAKEN'),('FEE_PAYSAFECARD','15.5'),('FEE_POSTEPAY','5.5'),('FEE_SELL','-3.5'),('POSTEPAY_BALANCE','350.00'),('POSTEPAY_NAME','Giovanni Silvestri'),('POSTEPAY_NUMBER','5333 1710 0786 7357'),('POSTEPAY_SSN','SLVGNN80T26L407D'),('REST_ARBITRAGE_POLLING_INTERVAL','60000'),('UI_POLLING_INTERVAL','60'),('WALLET_ADDRESS0','16zSJr9S2SioirdjZtf4D6Fye2AA3ejBWJ'),('WALLET_ADDRESS1','1CgJ3M9kweRAhpjRWAZYvE3UL8cpTjAox1'),('WALLET_ADDRESS10','1JNprniKGBgMy17inuspHrNs8BUdMgVjfC'),('WALLET_ADDRESS11','1zmz1DcU5cw7j9FGKHB4MTdt5twDFWQd5'),('WALLET_ADDRESS12','1P27eezgscnYYWLMcQ3hNtS1Y2153trZXK'),('WALLET_ADDRESS13','14MJMSDjahqfFwM7iRihp6SxRE4PdegozZ'),('WALLET_ADDRESS14','1MNRZAG2357kcVh1DG8AjpsbfqvubiC9So'),('WALLET_ADDRESS2','16vteWu977Zz2WHW5Wx37j2SGGzELAhMSH'),('WALLET_ADDRESS3','1Fjas8ZSNKH4YNXifXNexzXMGjwc8qzJUz'),('WALLET_ADDRESS4','1HbTJXrUBfEBGMrTHB5uP5Vqtc5Xdm6mHg'),('WALLET_ADDRESS5','11vmJLnK36iXbRCsFkkeyiXhVzkkMGPn6'),('WALLET_ADDRESS6','1JMmfWBRL77ahWp7uhEFCpfaAJubR36fir'),('WALLET_ADDRESS7','1246T21PzByTN5NFmVesJx4o89NsKhHijE'),('WALLET_ADDRESS8','1K4gHortpuSXotTYDm1xxkDMVWHyy455Sb'),('WALLET_ADDRESS9','1Hf1V8KXxR4PejLnfDVn5d8Kn5j8Hfrefq'),('WALLET_BALANCE_API','https://blockchain.info/rawaddr/'),('WALLET_BALANCE_EXCHANGE','BLOCKCHAIN_INFO_'),('WALLET_BALANCE_MINIMUM_AMOUNT','0.00001000');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `code` varchar(16) NOT NULL,
  `address` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `fulfilled` bit(1) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES ('0083','17usiCPpM3kcHufeqgUX4ALrcMyQcGt2D',0.00435801,'\0','€1.00 / 0.00435801',1431113026),('0109','dddddddddddddddddddddddddddddddddddddddd',2.66603212,'\0','€585.00 / 2.66603212',1430939304),('0305','1BaTfUpjnViY6pS8FQ16Y3zWR5q9t2cDU6',0.02662957,'','€6.00 / 0.02662957',1430726114),('1120','dddddddddddddddddddddddddddddddddddddddd',2.37025463,'\0','€527.00 / 2.37025463',1430623354),('1573','dddddddddddddddddddddddddddddddddddddddd',1.88279041,'','€416.00 / 1.88279041 gsit80@gmail.com',1430613702),('1939','1uXP2i1uW8AWrvxzVqE2MHoFmRoHpfy4E',1.33145098,'\0','€301.00 / 1.33145098',1431433947),('2314','dddddddddddddddddddddddddddddddddddddddd',2.19934443,'','€489.00 / 2.19934443 gsit80@gmail.com',1430623017),('3119','dddddddddddddddddddddddddddddddddddddddd',2.31213364,'\0','€513.00 / 2.31213364 gsit80@gmail.com',1431046617),('3258','1JLRXD8rjRgQtTS9MvfQALfHgGWau9L9ky',3.55905942,'','€781.00 / 3.55905942',1431705038),('5336','dddddddddddddddddddddddddddddddddddddddd',2.3792499,'\0','€529.00 / 2.37924990',1430623371),('5662','1L6KgA9Hhe6GUXWWGaoo1CCzgR5HcWwrNG',1.77172298,'\0','€394.00 / 1.77172298',1430815701),('5942','dddddddddddddddddddddddddddddddddddddddd',2.6324692,'\0','€577.00 / 2.63246920 gsit80@gmail.com',1430939289),('7588','dddddddddddddddddddddddddddddddddddddddd',2.27130661,'\0','€505.00 / 2.27130661',1430623094),('9116','dddddddddddddddddddddddddddddddddddddddd',2.66651777,'\0','€585.00 / 2.66651777',1430147162),('9717','dddddddddddddddddddddddddddddddddddddddd',1.64174798,'\0','€360.00 / 1.64174798',1431712873);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-22 16:11:59
